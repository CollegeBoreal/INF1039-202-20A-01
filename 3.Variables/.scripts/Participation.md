# Participation au 17-10-2020 10:01

## Légende

| Signe              | Signification                 |
|--------------------|-------------------------------|
| :heavy_check_mark: | Prêt à être corrigé           |
| :x:                | Projet inexistant             |

## Résultat

|:hash:| Boréal :id:                | :100:              |
|------|----------------------------|--------------------|
| 1 | [300106677](../300106677.py) | [:heavy_check_mark:](Correction.md#etudiant-300106677) |
| 2 | [300115064](../300115064.py) | [:heavy_check_mark:](Correction.md#etudiant-300115064) |
| 3 | [300115206](../300115206.py) | [:x:](Correction.md#etudiant-300115206) |
| 4 | [300117791](../300117791.py) | [:x:](Correction.md#etudiant-300117791) |
| 5 | [300117811](../300117811.py) | [:x:](Correction.md#etudiant-300117811) |
| 6 | [300121460](../300121460.py) | [:heavy_check_mark:](Correction.md#etudiant-300121460) |
| 7 | [300122014](../300122014.py) | [:heavy_check_mark:](Correction.md#etudiant-300122014) |
| 8 | [300122131](../300122131.py) | [:heavy_check_mark:](Correction.md#etudiant-300122131) |
